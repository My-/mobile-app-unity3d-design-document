
# Testing main menu
Feature: main-menu mode

Scenario: Selecting "Play" option
  Given game is running
  And main-menu mode active
  When user selects "Play"
  Then play-mode is activated
  And user able to play game

# Must have
Scenario: Selecting "About" option
  Given game is running
  And main-menu mode active
  When user selects "About"
  Then About-page is activated
  And user able to see developer details
  And user able to see designer details
  And user able to see artist details

Scenario: Selecting "Exit option"
  Given game is running
  And main-menu mode active
  When user selects "Exit"
  Then game is closed.

# Optional
Scenario: Selecting "Choose level" option
  Given game is running
  And main-menu mode active
  When user selects "Choose level"
  Then Choose level page is displayed
  And user able to select desired level


# Testing in game menu
Feature: in-game menu

Scenario: Going to in-game menu
  Given game is running
  And game is in play-mode
  When user hits "Esc" (desktop)
  Or user hits back (mobile)
  Then in-game menu is activated
  And in-game menu is displayed
  And game is paused

Scenario: Selecting "Resume" option
  Given game is running
  And game displaying in-game menu
  When user selects "Resume" option
  Then in-game menu is closed
  And game is un-paused
  And user can continue play the game

Scenario: Selecting "Controls" option
  Given game is running
  And game displaying in-game menu
  When user selects "Controls" option
  Then controls-page is displayed
  And user can see controls explanation/mappings

Scenario: Selecting "Main menu" option
  Given game is running
  And game displaying in-game menu
  When user selects "Main menu" option
  Then play mode is closed
  # Optional: And game state is saved
  And main-menu page is displayed


# Testing player movements
Feature: Player movement

Scenario: Player able to move
	Given game is in play mode
	And the character has the 'life'
	When any of movement buttons are pressed (desktop)
	Or touch control is moved (mobile)
	Then the character moves to that direction

Scenario: Player stays still
  Given game is in play mode
  And the character has NO 'life'
  When  any of movement buttons are pressed  (desktop)
  Or touch control is moved  (mobile)
  Then the character stays still


# Testing Shooting
Feature: Shooting 'live'

Scenario: transferring 'live' between characters
	Given game is in play mode
	And the character has 'life'
	When an inactive character is clicked (desktop)
	Or an inactive character is touched (mobile)
	Then 'life' is shotted to that character
	And active character becomes inactive
	And inactive character becomes active

Scenario: damaging enemy
	Given game is in play mode
  And the character has 'life'
  When an enemy is clicked (desktop)
  Or an enemy is touched (mobile)
	Then laser is shotted to that enemy
	And the damage is applied to the enemy

Scenario: killing enemy
	Given game is in play mode
  And the character has 'life'
  And an enemy has low health
  When an enemy is clicked (desktop)
  Or an enemy is touched (mobile)
	Then laser is shotted to that enemy
	And the critical damage is applied to the enemy
  And an enemy is destroyed

# Testing game play
Feature: Game play

Scenario: Getting to next level
  Given game is in play mode
  And the character has 'life'
  When character reaches lift
  Then level is completed
  And new level is loaded

Scenario: Wining game
  Given game is in play mode
  And the character has 'life'
  And current level is the last one
  When character reaches lift
  Then level is completed
  And game is completed
  And completed game page is displayed

Scenario: Loosing game
  Given game is in play mode
  And character has 'life'
  When an enemy touches active character
  Then 'life' are stolen
  And player looses the game

Scenario: Activating enemies
  Given game is in play mode
  And character has 'life'
  When character are near an enemy
  Then an enemy are start moving towards that character

# Optional
Scenario: Charging battery
  Given game is in play mode
  And character has 'life'
  When character moves on top of "charging port"
  Then battery is charged
  And character moves faster
